import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular'; //, IonicPage
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

//../../validators
import { UsernameValidator } from './username.validator';
import { WsAuthenticate } from '../../providers/authenticate.service';
import { CatalogPage } from '../catalog/catalog';

@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
	providers: [WsAuthenticate]
})
export class LoginPage {
	//Define FormBuilder / model properties / Data Entry
	public form: FormGroup;
	public email: any = 'lor@em';
	public password: any = 'Ipsum1234';

	constructor(
		public navCtrl: NavController,
		public alertCtrl: AlertController,
		public fb: FormBuilder,
		public wsAuthenticate: WsAuthenticate) {
		//let item = this.navParams.get('item');
		//console.log('item: ' + item)
		// Create the form builder validation rules
		this.form = fb.group({
			"email": ["", Validators.compose(
				[
					UsernameValidator.validUsername,
					Validators.required,
					Validators.email,
					Validators.minLength(5),
					Validators.maxLength(25),
				])],
			"password": ["", Validators.compose([
				Validators.required,
				Validators.minLength(9),
				//this is for the letters (both uppercase and lowercase) and numbers validation
				Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
			])]
		});
	}

	ionViewWillEnter()
	{
	}

	// Clear values in the page's HTML form fields
	resetFields(): void
	{
		this.email = "";
		this.password = "";
	}

	authenticate(): void
	{
		this.wsAuthenticate.authenticate(this.email, this.password)
			.then(data => {
				if (data['status'] === 1) {
					this.navCtrl.push(
						CatalogPage,
						{
							firstName: data['firstName'],
							lastName: data['lastName']
						});

				}
				//otherwise let 'em know anyway
				else {
					this.showDialog('Email o password invalido');
				}
			}
			);

	}

	signIn(): void {
		let alert = this.alertCtrl.create({
			title: 'Registrarse',
			message: 'Registrarse 22?',
			inputs: [
				{
					name: 'firstName',
					placeholder: 'Nombres'
				},
				{
					name: 'lastName',
					placeholder: 'Apellidos'
				},
				{
					name: 'username',
					placeholder: 'Usuario'
				},
				{
					name: 'password',
					placeholder: 'Contraseña',
					type: 'password'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Se collonió');
					}
				},
				{
					text: 'Sí',
					handler: data => {
						this.wsAuthenticate.signIn(data.firstName, data.lastName, data.username, data.password)
						.then(data => {
								console.log(data);
								console.log("Respuesta signIn: " + data['status']);
								
								//If the request was successful notify the user
								if (data['status'] === 1)
								{
									this.showDialog('Su usuario fue creado exitosamente');
								}
								//otherwise let 'em know anyway
								else
								{
									this.showDialog('Email o password invalido');
								}
							}
						);
									
					}
				}
			]
		});
		alert.present();
	}

	sendNotification(message): void {
		/*
		let notification = this.toastCtrl.create({
			message: message,
			duration: 3000
		});
		notification.present(message);
		*/
		this.showDialog(message);
	}

	public showDialog(message) {
		let alert = this.alertCtrl.create({
			title: 'Information',
			subTitle: message,
			buttons: ['OK']
		});
		alert.present();
	}

}
