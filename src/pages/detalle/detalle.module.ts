import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DetalleInventario} from './detalle';

@NgModule({
	declarations: [
		DetalleInventario,
	],
	imports: [
		IonicPageModule.forChild(DetalleInventario)
	],
	exports: [
		DetalleInventario
	]
})
export class DetalleModule {}