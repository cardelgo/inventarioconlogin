import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Notifier } from '../../providers/notifier.service';
import { WsInventario } from '../../providers/inventario.service';


@IonicPage()
@Component({
  selector: 'page-add-inventario',//page-detalle
  templateUrl: 'detalle.html',
  providers: [WsInventario]
})
export class DetalleInventario {
	//Define FormBuilder / model properties / Data Entry
	public form : FormGroup;
	public producto : any;
	public precio : any;
	public existencia : any;
	public proveedor : any;
	public foto : any;
	
	//property to store the recordID for when an existing ent...
	public recordID : any = null;
	
	//Flag to be user for checking whether we are adding/edit..
	public isEdited : boolean = false;
	
	//Flag to hide the form upon succesful completion of remote operation
	public hideForm : boolean = false;
	
	//Property to help ste page title
	public pageTitle : string;
	
	constructor(
		public navCtrl: NavController,
		public alertCtrl: AlertController,
		public NP: NavParams,
		public fb: FormBuilder,
		public notifier : Notifier,
		public wsInventario : WsInventario)
	{
		//let item = this.navParams.get('item');
		//console.log('item: ' + item)
		// Create the form builder validation rules
		this.form = fb.group({
			"producto" : ["", Validators.required],
			"existencia" : ["", Validators.required],
			"precio" : ["", Validators.required],
			"proveedor" : ["", Validators.required],
			"foto" : ["", Validators.required]
		});
	}

	ionViewWillEnter()
	{
		this.resetFields();
		if (this.NP.get("record"))
		{
			this.isEdited = true;
			this.selectEntry(this.NP.get("record"));
			this.pageTitle = 'Detalle Producto';
		}
		else
		{
			this.isEdited = false;
			this.pageTitle = 'Creación de Producto';
		}
	}
	
	// Clear values in the page's HTML form fields
	resetFields() : void
	{
		this.producto = "";
		this.existencia = "";
		this.precio = "";
		this.proveedor = "";
		this.foto = "";		
	}
	
	// Assign the navigation retrieved data to properties
	// used as models on the page's HTML form
	selectEntry(item)
	{
		this.producto = item.producto;
		this.existencia = item.existencia;
		this.precio = item.precio;
		this.proveedor = item.proveedor;
		this.foto = item.foto;
		this.recordID = item.id;
	}
	
	saveEntry()
	{
		let producto : string = this.form.controls["producto"].value,
			existencia : string = this.form.controls["existencia"].value,
			precio : string = this.form.controls["precio"].value,
			proveedor : string = this.form.controls["proveedor"].value,
			foto : string = this.form.controls["foto"].value;
		
		if (this.isEdited)
		{
			this.updateEntry(producto, existencia, precio, proveedor, foto);
		}
		else
		{
			this.createEntry(producto, existencia, precio, proveedor, foto);
		}
	}
	
	updateEntry(producto, existencia, precio, proveedor, foto) {
		let alert = this.alertCtrl.create({
		  title: 'Confirmar actualización',
		  message: 'Desea actualizar este producto?',
		  buttons: [
			{
			  text: 'Cancel',
			  role: 'cancel',
			  handler: () => {
				console.log('Se collonió');
			  }
			},
			{
			  text: 'Sí',
			  handler: () => {
				this.doUpdateEntry(producto, existencia, precio, proveedor, foto);
			  }
			}
		  ]
		});
		alert.present();
	  }

	doUpdateEntry(producto, existencia, precio, proveedor, foto)
	{

		console.log('Entrando a actualizar');
		//let name : string = this.form.controls["producto"].value;
		this.wsInventario.updateEntry(producto, existencia, precio, proveedor, foto, this.recordID)
			.then(data => {
				console.log(data);
				console.log("Respuesta update: " + data['status']);
				if(data['status'] === 1) //200
				{
					this.hideForm = true;
					this.notifier.send('El producto: ' + producto + ' fue actualizado');
				}
				//Otherwise let 'em know anyway
				else
				{
					this.notifier.send('Something went wrong!');
				}
			})
	}

	createEntry(producto, existencia, precio, proveedor, foto)
	{
		console.log('Entrando a crear');
		this.wsInventario.createEntry(producto, existencia, precio, proveedor, foto)
			.then(data => {
				console.log(data);
				console.log("Respuesta create: " + data['status']);
				if(data['status'] === 1)
				{
					this.hideForm = true;
					this.notifier.send('producto: ' + producto + ' fue creado');
				}
				//Otherwise let 'em know anyway
				else
				{
					this.notifier.send('Something went wrong!');
				}
			})
	}
	
}
