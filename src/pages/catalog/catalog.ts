import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, NavParams} from 'ionic-angular'; //, IonicPage

import { Notifier } from '../../providers/notifier.service';
import { WsInventario } from '../../providers/inventario.service';
import { LoginPage } from '../login/login';
import { FormControl } from '@angular/forms';

@Component({
	selector: 'page-catalog',
	templateUrl: 'catalog.html',
	providers: [WsInventario]
})
export class CatalogPage {
	public items: any = []; //arreglo de datos
	public firstName: string = '';
	public lastName: string = '';
	itemsTemp : any = [];
	searchTerm : string = '';
	searchControl : FormControl;
	searching : any = false;

	constructor(
		public navCtrl: NavController,
		public wsInventario: WsInventario,
		public alertCtrl: AlertController,
		public notifier: Notifier,
		public navParams: NavParams,
		public loading: LoadingController)
	{
		this.firstName = navParams.get('firstName');
		this.lastName = navParams.get('lastName');
		this.searchControl = new FormControl();

	}

	ionViewWillEnter() {
		console.log('Iniciando ionViewWillEnter...');
		this.loadData();
	}

	ionViewWillLoaded() {
		console.log('Iniciando ionViewWillLoaded...');
		this.loadData();
	}

	loadData() {
		//this.items = [];

		let loader = this.loading.create({
			content: `
			<div class="custom-spinner-container">
				<div class="custom-spinner-box"></div>
			</div>`
		});

		//Refrescamos los datos del arreglo

		loader.present().then(() => {
			this.wsInventario.getAllInventario()
				.then(data => {
					this.items = data;
					this.itemsTemp = data;
				});
			loader.dismiss();
		});

	}//Fin de carga de datos

	doRefresh(refresher) {
		this.loadData();
		setTimeout(() =>
		{
			refresher.complete();
		},
		2000);
	}

	//Allow navigation to the Inventario page for creating a new entry
	addEntry() {
		this.navCtrl.push('DetalleInventario');
	}

	viewEntry(param) {
		this.navCtrl.push('DetalleInventario', param);
	}

	deleteEntry(objRecord)
	{
		let alert = this.alertCtrl.create({
			title: 'Confirmar eliminación',
			message: 'Desea eliminar el producto ' + objRecord.record.producto + '?',
			buttons: [
			  {
				text: 'Cancel',
				role: 'cancel',
			  },
			  {
				text: 'Sí',
				handler: () => {
				  this.doDeleteEntry(objRecord);
				}
			  }
			]
		  });
		  alert.present();
	  }
	
	doDeleteEntry(objRecord) {
		let producto: string = objRecord.record.producto;

		this.wsInventario.deleteEntry(objRecord.record.id)
			.then(data => {
				console.log(data);
				console.log("Respuesta Delete: " + data['status']);

				//If the request was successful notify the user
				if (data['status'] === 1) {
					this.notifier.send('El producto ' + producto + ' fue eliminado');

					//Quito el producto de la lista
					let index = this.items.indexOf(objRecord.record);

					if (index > -1) {
						this.items.splice(index, 1);
					}
				}
				//otherwise let 'em know anyway
				else {
					this.notifier.send('Algo inesperado ha pasado');
				}
			}
			);

	}


	exit()
	{
		let alert = this.alertCtrl.create({
			title: 'Confirmar salida',
			message: 'Desea salir de la aplicación?',
			buttons: [
			  {
				text: 'Cancel',
				role: 'cancel',
				handler: () => {
				  console.log('No salir');
				}
			  },
			  {
				text: 'Sí',
				handler: () => {
					this.navCtrl.push(LoginPage);
				}
			  }
			]
		  });
		  alert.present();
	}

	onSearchInput()
	{
		console.log('onSearchInput')
		this.searching = true;
		this.setFilteredItems();
	}

	setFilteredItems()
	{
		//Refresca la lista con la lista de datos cargada en loadData
		this.items = this.itemsTemp;

		//Activa el spinner poniendo searching = true;
		this.searching = true;

		//Si la SearchBox exta vacia regresa al estado original la lista
		//con los datos que inicialmente cargo loadData
		if(this.searchTerm == '')
		{
			this.items = this.itemsTemp;
			this.searching = false;
			return;
		}

		this.items = this.items.filter((item) => 
			{
				//filtra por el campo nombre del producto
				if(item.producto.toLowerCase().indexOf(this.searchTerm.toLowerCase())>-1)
				{
					this.searching = false;
					return true;
				}
				this.searching = false;
				return false;
		});

	}

	
	/*
	ionViewDidLoad()
	{
		//Crear timer que se dispara filtro
		//cuando encuentra un cambio en el SearchBox
		//.debounceTime(700)
		this.searchControl.valueChanges.subscribe(search =>
		{
			console.log('Value change ' + search);
			this.searching = false;
			this.setFilteredItems();
		});
	}
	*/
	

}

