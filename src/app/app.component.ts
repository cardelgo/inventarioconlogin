
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { CatalogPage } from '../pages/catalog/catalog';
import { ContactPage } from '../pages/contact/contact';
import { AboutPage } from '../pages/about/about';
//import { Title } from '@angular/platform-browser';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav : Nav;
  rootPage:any = LoginPage;
  pages: Array<{title: string, component: any, icon: any}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.pages = [
      {title: 'Inicio', component: CatalogPage, icon: 'home'},
      {title: 'Contacto', component: ContactPage, icon: 'help'},
      {title: 'About', component: AboutPage, icon: 'home'}
    ]
  }

  openPage(page)
  {
    //Reset the content nav to have just this page
    //we wouldn't want the Back button on this scenario
    this.nav.setRoot(page.component);
  }
}
