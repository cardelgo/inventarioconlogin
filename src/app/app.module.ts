import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';  
import { LoginPage } from '../pages/login/login';
import { CatalogPage } from '../pages/catalog/catalog';
import { ContactPage } from '../pages/contact/contact';
import { AboutPage } from '../pages/about/about';

import { WsConstants } from '../providers/webconstants.service';
import { Notifier } from '../providers/notifier.service';
import { WsInventario } from '../providers/inventario.service';
import { WsAuthenticate } from '../providers/authenticate.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    LoginPage,
    CatalogPage,
    ContactPage,
    AboutPage,
    MyApp,
  ],
  imports: [
    BrowserModule,
	  HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    LoginPage,
    CatalogPage,
    ContactPage,
    AboutPage,
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Notifier,
    WsConstants,
    WsInventario,
    WsAuthenticate
  ]
})
export class AppModule {}
