import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';
import { WsConstants } from './webconstants.service';

@Injectable()
export class WsInventario {
	data : any;

	constructor(public http: Http, public wsConstants: WsConstants){
		console.log('Hello WsInventario provider');
	}
	
	getAllInventario(){
		let opt : RequestOptions;
		let myHeaders : Headers = new Headers;
		myHeaders.set('Accept', 'application/json; chartset=utf-8');
		opt = new RequestOptions({
			headers : myHeaders
		})// fin opt
		
		return new Promise (resolve => {
			this.http.get(this.wsConstants.baseURI + "?key=query", opt)
				.map(res => res.json())
				.subscribe(data => {
					this.data = data
					resolve(this.data);
				})
			}
		)// fin Promise
	}// fin getAllInventario

	createEntry(producto, existencia, precio, proveedor, foto)
	{
		let body : string = "key=create&producto=" + producto + "&existencia=" + existencia +
				"&precio=" + precio + "&proveedor=" + proveedor + "&foto=" + foto,
			type : string = "application/x-www-form-urlencoded; charset=UTF-8",
			headers : any = new Headers({'Content-Type': type}),
			options : any = new RequestOptions({headers: headers}),
			url : any = this.wsConstants.baseURI;
			
		return new Promise (resolve => {
			this.http.post(url, body, options)
				.map(res => res.json())
				.subscribe(data => {
					this.data = data;
					resolve (this.data);
				})
		})
	}// Fin ceate

	updateEntry(producto, existencia, precio, proveedor, foto, recordID)
	{
		let body : string = "key=update&producto=" + producto + "&existencia=" + existencia +
				"&precio=" + precio + "&proveedor=" + proveedor + "&foto=" + foto +
				"&recordId=" + recordID,
			type : string = "application/x-www-form-urlencoded; charset=UTF-8",
			headers : any = new Headers({'Content-Type': type}),
			options : any = new RequestOptions({headers: headers}),
			url : any = this.wsConstants.baseURI;
			
		console.log('Entrando a Actualissar Servicio con ' + body);
		return new Promise (resolve => {
			this.http.post(url, body, options)
				.map(res => res.json())
				.subscribe(data => {
					console.log("Status: " + data.status);
					this.data = data;
					resolve (this.data);
				},
				error => {
					console.error(error);
					resolve({status:-1});
				})
		})
	}// Fin update

	deleteEntry(recordID)
	{
		let body : string = "key=delete&recordId=" + recordID,
			type : string = "application/x-www-form-urlencoded; charset=UTF-8",
			headers : any = new Headers({'Content-Type': type}),
			options : any = new RequestOptions({headers: headers}),
			url : any = this.wsConstants.baseURI;
			
		console.log('Entrando a Eliminar Servicio');
		return new Promise (resolve => {
			this.http.post(url, body, options)
				.map(res => res.json())
				.subscribe(data => {
					console.log("Status: " + data.status);
					this.data = data;
					resolve (this.data);
				})
		})
	}// Fin Delete

}// Fin de clase
