import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';
import { WsConstants } from './webconstants.service';

@Injectable()
export class WsAuthenticate {
	data : any;
	constructor(public http: Http, public wsConstants: WsConstants)
	{
		console.log('Hello WsAuthenticate provider');
	}
	
	authenticate(email, password)
	{
		let opt : RequestOptions;
		let myHeaders : Headers = new Headers;
		myHeaders.set('Accept', 'application/json; chartset=utf-8');
		opt = new RequestOptions({
			headers : myHeaders
		})// fin opt
		
		return new Promise (resolve => {
			this.http.get(this.wsConstants.baseURI + "?key=authenticate&email=" + email + "&password=" + password , opt)
				.map(res => res.json())
				.subscribe(data => {
					this.data = data
					resolve(this.data);
				})
			}
		)
	}	

	signIn(nombres, apellidos, email, password)
	{
		let body : string = "key=signin&nombres=" + nombres + "&apellidos=" + apellidos +
				"&email=" + email + "&password=" + password,
			type : string = "application/x-www-form-urlencoded; charset=UTF-8",
			headers : any = new Headers({'Content-Type': type}),
			options : any = new RequestOptions({headers: headers}),
			url : any = this.wsConstants.baseURI;
			
		console.log("Inicia signIn con " + url  + " --  " + body);
		return new Promise (resolve => {
			this.http.post(url, body, options)
				.map(res => res.json())
				.subscribe(data => {
					this.data = data;
					resolve (this.data);
				})
		})
	}
	
}// Fin de clase
