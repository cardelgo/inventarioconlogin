import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class Notifier {

	constructor(public toastCtrl: ToastController)
	{
		console.log('Hello WsAuthenticate provider');
	}
    
	send(message) : void
	{		
		let notification = this.toastCtrl.create({
			message: message,
			duration: 3000
		});
        notification.present();
        
        /*
		let alert = this.alertCtrl.create({
			title: '<ion-icon ios="ios-information-circle" md="md-information-circle"></ion-icon>Inform',
			subTitle: message,
			buttons: ['OK']
		});
		alert.present();
        */        
	}	
}
